<?php
/**
 * Template Name: Our Product
 *
 * Template for displaying a blank page.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined('ABSPATH') || exit;
?>
<!--START HEADER-->
<?php get_header() ?>
<!--END HEADER-->
<section class="our-project-heading container-fluid">
    <div class="content">
        <h2>Our Projects</h2>
        <div class="sub">
            <ul>
                <?php
                $terms = get_terms(array('taxonomy' => 'projects', 'hide_empty' => false));
                foreach ($terms as $item) {
                    $isActive = $item->term_id == get_queried_object()->term_id ? 'class="active"' : '';
                    echo '<li '.$isActive.'><a href="'.get_term_link($item->term_id).'">' . $item->name . '</a></li>';
                }
                ?>
            </ul>
        </div>
    </div>
</section>
<section class="our-project-list container-fluid">
    <div class="content">
        <!-- FIRST PROJECT WILL FULL SCREEN-->
        <?php while (have_posts()) : the_post() ?>
            <div class="item">
                <div class="item-box" data-href="<?php the_permalink(); ?>">
                    <div class="project-heading">
                        <p><?= tr_posts_field('address') ?></p>
                        <h5><?php the_title(); ?></h5>
                        <a class="btn btn-sm btn-primary radius-5">Residences</a>
                    </div>
                    <div class="thumbnail">
                        <?= wp_get_attachment_image((int)tr_posts_field('thumbnail'), 'large'); ?>
                    </div>
                </div>
            </div>
        <?php endwhile; ?>
    </div>
</section>


<!--START FOOTER-->
<?php get_footer() ?>
<!--END FOOTER-->

<script>
    $('.btn.btn-scroll-top').click(function () {
        $([document.documentElement, document.body]).animate({
            scrollTop: $("body").offset().top - 140
        }, 2000);
    })

    $('.our-project-list .content .item-box').click(function () {
        window.location = $(this).attr('data-href')
    })
</script>