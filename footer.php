<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

// Exit if accessed directly.
defined('ABSPATH') || exit;

$container = get_theme_mod('understrap_container_type');
if (!is_front_page()) { ?>
    <footer class="footer">
        <div class="main">
            <div class="logo">
                <img src="<?= get_template_directory_uri() ?>/assets/images/Logo.png" alt="">
            </div>
            <div class="menu">
                <ul>
                    <li>6 Handy Road, #02-01, The Luxe<br>Singapore 229234</li>
                </ul>
            </div>
        </div>
        <div class="-copyright">
            <p>© 2020 JBE Holdings Pte Ltd. All Rights Reserved.</p>
        </div>
        <button class="btn btn-scroll-top"><i class="fa fa-angle-up" aria-hidden="true"></i></button>
    </footer>
    <?php
}
wp_footer();
?>
<script>
    // Initialize ANIMATION
    AOS.init({
        once: true
    });

    $(document).ready(function () {
        <!--  FAKE SCROLL FOR ANIMATION -->
        $("body,html").animate({
            scrollTop: 1
        }, 0)
    })
</script>
</body>
</html>