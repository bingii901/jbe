<!-- Start of Future Parallax Scene -->
<section class="future-parallax">
    <div class="parallax-window" data-parallax="scroll"
         data-image-src="<?= (wp_get_attachment_image_src($data['image'], 'large')[0]) ?>"></div>
</section>
<script>
    $('.future-parallax .parallax-window').parallax();
</script>
<!-- Stop of Future Parallax Scene-->