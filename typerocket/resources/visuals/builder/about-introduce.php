<!-- Start of About Introduce Scene -->
<section class="about-introduce container-fluid">
    <div class="content paragraph">
        <h1><?= $data['headline'] ?></h1>
        <div class="row">
            <div class="col-xl-6">
                <h4 data-aos="fade-up"><?= $data['sub-headline'] ?></h4>
                <div data-aos="fade-down">
                    <?php
                    if (!empty($data['description']))
                        foreach ($data['description'] as $item) { ?>
                            <p><?= $item['line'] ?></p><br>
                        <?php } ?>
                </div>
                <br>
            </div>
            <div class="offset-xl-1 col-xl-5">
                <div class="image" data-aos="flip-down">
                    <?= wp_get_attachment_image((int)$data['image'], array(1000, 1000)); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Stop of About Introduce Scene-->