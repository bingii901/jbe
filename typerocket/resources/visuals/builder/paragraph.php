<!-- Start of Paragraph Scene -->
<?php if (!$data['headline_position']) { ?>
    <section
            class="expertise-individual-executive container-fluid<?php if (!$data['background']) echo ' bg-gray-light'; ?>">
        <div class="content">
            <div class="paragraph">
                <h5><?= $data['headline'] ?><h5><br>
                        <?= $data['paragraph'] ?>
            </div>
        </div>
    </section>
<?php } else { ?>
    <section class="expertise-individual-executive container-fluid">
        <div class="content">
            <h4 class="heading"><?= $data['headline'] ?></h4>
            <div class="paragraph">
                <?= $data['paragraph'] ?>
            </div>
        </div>
    </section>
<?php } ?>
<!-- Stop of Paragraph Scene-->