<!-- Start of Quote Scene -->
<section class="expertise-individual-heading container-fluid">
    <div class="content quote">
        <h3 data-author="<?= $data['author'] ?>"><?= $data['quote'] ?></h3>
    </div>
</section>
<!-- Stop of Quote Scene-->