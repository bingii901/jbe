<!-- Start of About Counter Scene -->
<section class="about-counter container-fluid bg-gray-light">
    <div class="content" data-aos="flip-up">
        <div class="heading">
            <h3><?= $data['headline'] ?></h3>
            <p><?= $data['description'] ?></p>
        </div>
        <div class="box-parameter" data-aos="flip-up" data-aos-id="super-counter">
            <?php
            if (!empty($data['counter']))
                foreach ($data['counter'] as $item) { ?>
                    <div class="item">
                        <div class="icon">
                            <img src="<?= (wp_get_attachment_image_src($item['icon'], 'small')[0]) ?>" alt="Icon">
                        </div>
                        <div class="parameter">
                            <h3 class="counter-count <?= $item['type'] ?>"><?= $item['number'] ?></h3>
                            <p><?= $item['description'] ?></p>
                        </div>
                    </div>
                <?php } ?>
        </div>
    </div>
</section>
<script>
    document.addEventListener('aos:in:super-counter', ({detail}) => {
        $('.counter-count').each(function () {
            $(this).prop('Counter', 0).animate({
                Counter: $(this).text()
            }, {
                duration: 1500,
                easing: 'swing',
                step: function (now) {
                    $(this).text(Math.ceil(now));
                }
            });
        });
    });
</script>
<!-- Stop of About Counter Scene-->