<!-- Start of Future Information Scene -->
<section class="contact-page-future container-fluid bg-gray-light">
    <div class="content row">
        <div class="col-lg-6">
            <h4><?= $data['headline'] ?></h4>
        </div>
        <div class="information col-lg-5 offset-lg-1">
            <div class="list">
                <h5>Phone</h5>
                <p>+65 6338 3616</p>
            </div>
            <div class="list">
                <h5>Address</h5>
                <p>6 Handy Road, #02-01, The Luxe <br>Singapore 229234</p>
            </div>
        </div>
    </div>
</section>
<!-- Stop of Future Information Scene-->