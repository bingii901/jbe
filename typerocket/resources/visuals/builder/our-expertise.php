<!-- Start of Our Expertise Scene -->
<section class="expertise-heading container-fluid">
    <div class="content">
        <h1><?= $data['headline'] ?></h1>
        <p><?= $data['sub_headline'] ?></p>
    </div>
</section>
<section class="expertise-list container-fluid">
    <div class="content">
        <div class="item" data-href="#">
            <div class="item-box">
                <div class="heading">
                    <h5>Private Condo</h5>
                    <a class="btn btn-lg btn-primary" href="#">View More</a>
                </div>
                <div class="thumbnail">
                    <img src="<?= get_template_directory_uri() ?>/assets/images/project/Past%20Projects/Private%20Condo/The%20Luxe/6%20handy%20rd.jpg" alt="">
                </div>
            </div>
        </div>
        <div class="item" data-href="#">
            <div class="item-box">
                <div class="heading">
                    <h5>Executive Condo</h5>
                    <a class="btn btn-lg btn-primary" href="#">View More</a>
                </div>
                <div class="thumbnail">
                    <img src="<?= get_template_directory_uri() ?>/assets/images/project/Past%20Projects/EC/Signature%20at%20Yishun/IMG-20170826-WA0009.jpg" alt="">
                </div>
            </div>
        </div>
        <div class="item" data-href="#">
            <div class="item-box">
                <div class="heading">
                    <h5>Landed Housing</h5>
                    <a class="btn btn-lg btn-primary" href="#">View More</a>
                </div>
                <div class="thumbnail">
                    <img src="<?= get_template_directory_uri() ?>/assets/images/project/Past%20Projects/Landed%20Housing/Terrace%20houses.PNG" alt="">
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Stop of Our Expertise Scene-->