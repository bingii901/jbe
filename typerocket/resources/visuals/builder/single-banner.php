<!-- Start of Single Banner Scene -->
<section class="single-banner">
    <div class="swiper-container">
        <div class="heading">
            <div class="typing active">
                <h1><?= $data['headline'] ?></h1>
            </div>
            <p><?= $data['description'] ?></p>
            <a class="btn btn-lg btn-primary" href="#">Our Projects</a>
        </div>
        <div class="swiper-wrapper">
            <?php
            if (!empty($data['images_list'])) {
                foreach ($data['images_list'] as $item) { ?>
                    <div class="swiper-slide">
                        <?= wp_get_attachment_image((int)$item['image'], array(1920, 1080)); ?>
                    </div>
                <?php }
            } ?>
        </div>
        <!-- Add Pagination -->
        <div class="swiper-pagination"></div>
    </div>
</section>
<script>
    $('.navbar').addClass('is-home')
    $('body').css('margin-top', 0)
</script>
<!-- Stop of Single Banner Scene-->