<!-- Start of Future Normal Scene -->
<section class="future-parallax<?php if($data['padding']) echo ' container-fluid'; ?>">
    <img src="<?= (wp_get_attachment_image_src($data['image'], 'large')[0]) ?>" alt="">
</section>
<!-- Stop of Future Normal Scene-->