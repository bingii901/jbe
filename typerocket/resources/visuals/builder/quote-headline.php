<!-- Start of Quote Scene -->
<section class="about-introduce container-fluid">
    <div class="content quote">
        <h4 data-aos="fade-up"><?= $data['headline'] ?></h4>
        <p data-author="<?= $data['author'] ?>" data-aos="fade-down">“<?= $data['quote'] ?>”</p>
    </div>
</section>
<!-- Stop of Quote Scene-->