<!-- Start of Future Project Scene -->
<section class="expertise-individual-future-project container-fluid">
    <h5><?= $data['headline'] ?></h5>
    <div class="content row">
        <div class="item col-lg-3 col-md-6">
            <div class="thumbnail">
                <img src="<?= get_template_directory_uri() ?>/assets/images/handy-026238.jpg" alt="">
            </div>
            <div class="heading">
                <p>6 Handy Road</p>
                <h5>The Luxe</h5>
                <a class="btn btn-sm btn-primary" href="#">Apartment</a>
            </div>
        </div>
        <div class="item col-lg-3 col-md-6">
            <div class="thumbnail">
                <img src="<?= get_template_directory_uri() ?>/assets/images/Rectangle%201942@2x.jpg" alt="">
            </div>
            <div class="heading">
                <p>PASIR PANJANG ROAD</p>
                <h5>The Ville</h5>
                <a class="btn btn-sm btn-primary" href="#">Apartment</a>
            </div>
        </div>
        <div class="item col-lg-3 col-md-6">
            <div class="thumbnail">
                <img src="<?= get_template_directory_uri() ?>/assets/images/Rectangle%201956@2x.jpg" alt="">
            </div>
            <div class="heading">
                <p>INGGU ROAD / PENAGA PLACE</p>
                <h5>Terrace Houese</h5>
                <a class="btn btn-sm btn-primary" href="#">Apartment</a>
            </div>
        </div>
        <div class="item col-lg-3 col-md-6">
            <div class="thumbnail">
                <img src="<?= get_template_directory_uri() ?>/assets/images/FRM_4678.jpg" alt="">
            </div>
            <div class="heading">
                <p>346 SEMBAWANG CRESCENT</p>
                <h5>skypark residences</h5>
                <a class="btn btn-sm btn-primary" href="#">Apartment</a>
            </div>
        </div>
    </div>
</section>
<!-- Stop of Future Project Scene-->