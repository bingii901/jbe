<!-- Start of Contact Scene -->
<section class="contact-page-form contact-form container-fluid">
    <div class="content row">
        <div class="image col-lg-6">
            <img src="<?= wp_get_attachment_image((int)$data['image'], array(1000, 1000)); ?>" alt="">
        </div>
        <div class="form offset-lg-1 col-lg-5">
            <h3><?= $data['headline'] ?></h3>
            <p><?= $data['paragraph'] ?></p>

            <form class="form-label-float">
                <div class="form-row">
                    <div class="form-group col-md-12" data-aos="fade-right">
                        <input type="name" class="form-control" id="fname" required placeholder="First Name">
                        <span data-toggle="First Name*" data-required="This field is required"></span>
                    </div>
                    <div class="form-group col-md-12" data-aos="fade-left">
                        <input type="name" class="form-control" id="lname" required placeholder="Last Name">
                        <span data-toggle="Last Name*" data-required="This field is required"></span>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12" data-aos="fade-right">
                        <input type="email" class="form-control" id="email" required placeholder="Email Address">
                        <span data-toggle="Email Address*" data-required="This field is required"></span>
                    </div>
                    <div class="form-group col-md-12" data-aos="fade-left">
                        <input type="phone" class="form-control" id="phone" required placeholder="Phone Number">
                        <span data-toggle="Phone Number*" data-required="This field is required"></span>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group" data-aos="fade-right">
                    <textarea type="text" class="form-control" id="message" required
                              placeholder="Messages..."></textarea>
                        <span data-toggle="Message..." data-required="This field is required"></span>
                    </div>
                </div>
                <button type="submit" class="btn btn-submit" data-aos="fade-up">Submit</button>
            </form>
        </div>
    </div>
</section>

<!-- Stop of Contact Scene-->