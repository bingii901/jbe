<!-- Start of Project Scene -->
<section class="our-project-heading container-fluid">
    <div class="content">
        <h2><?= $data['headline'] ?></h2>
        <div class="sub">
            <ul>
                <li><a href="#">Ongoing Projects</a></li>
                <li class="active"><a href="#">Complete Projects</a></li>
            </ul>
        </div>
    </div>
</section>
<section class="our-project-list container-fluid">
    <div class="content">
        <!-- FIRST PROJECT WILL FULL SCREEN-->
        <div class="item">
            <div class="item-box" data-href="project-detail.php">
                <div class="project-heading">
                    <p>346 SEMBAWANG CRESCENT</p>
                    <h5>Skypark Residences</h5>
                    <a class="btn btn-sm btn-primary radius-5">Residences</a>
                </div>
                <div class="thumbnail">
                    <img src="<?= get_template_directory_uri() ?>/assets/images/project/Past%20Projects/EC/SkyPark%20Residences/Perspectives/view01_final.jpg"
                         alt="">
                </div>
            </div>
        </div>
        <div class="item">
            <div class="item-box" data-href="project-detail.php">
                <div class="project-heading">
                    <p>PASIR PANJANG ROAD</p>
                    <h5>Luxe Ville</h5>
                    <a class="btn btn-sm btn-primary radius-5">Residences</a>
                </div>
                <div class="thumbnail">
                    <img src="<?= get_template_directory_uri() ?>/assets/images/project/Past%20Projects/Private%20Condo/Luxe%20Ville/Luxe%20Ville.JPG"
                         alt="">
                </div>
            </div>
        </div>
        <div class="item">
            <div class="item-box" data-href="project-detail.php">
                <div class="project-heading">
                    <p>INGGU ROAD / PENAGA PLACE / WAK HASSAN DRIVE</p>
                    <h5>Terrace Houses</h5>
                    <a class="btn btn-sm btn-primary radius-5">Residences</a>
                </div>
                <div class="thumbnail">
                    <img src="<?= get_template_directory_uri() ?>/assets/images/project/Past%20Projects/Landed%20Housing/Sembawang%20Terrace%20Houses.JPG"
                         alt="">
                </div>
            </div>
        </div>
        <div class="item">
            <div class="item-box" data-href="project-detail.php">
                <div class="project-heading">
                    <p>6 HANDY ROAD</p>
                    <h5>The Luxe</h5>
                    <a class="btn btn-sm btn-primary radius-5">Residences</a>
                </div>
                <div class="thumbnail">
                    <img src="<?= get_template_directory_uri() ?>/assets/images/project/Past%20Projects/Private%20Condo/The%20Luxe/6%20handy%20rd.jpg"
                         alt="">
                </div>
            </div>
        </div>
        <div class="item">
            <div class="item-box" data-href="project-detail.php">
                <div class="project-heading">
                    <p>YISHUN</p>
                    <h5>Signature at Yishun</h5>
                    <a class="btn btn-sm btn-primary radius-5">Residences</a>
                </div>
                <div class="thumbnail">
                    <img src="<?= get_template_directory_uri() ?>/assets/images/project/Past%20Projects/EC/Signature%20at%20Yishun/Perspective%20(by%20Sixtree)/Clubhouse%20Aug%205.jpg"
                         alt="">
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $('.our-project-list .content .item').click(function () {
        window.location = $(this).attr('data-href')
    })
</script>
<!-- Stop of Project Scene-->