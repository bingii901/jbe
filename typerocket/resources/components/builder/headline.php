<h1>Headline Single</h1>
<?php
$baseGroup = $form->getGroup();

$options = [
    'H1' => 'h1',
    'H2' => 'h2',
    'H3' => 'h3',
    'H4' => 'h4',
    'H5' => 'h5',
];

echo $form->text('Headline');
echo $form->radio('Size')->setOptions($options);