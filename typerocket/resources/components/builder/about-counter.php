<h1>About Counter</h1>
<?php
$baseGroup = $form->getGroup();

$options = [
    'Plus' => 'plus',
    'Over' => 'over',
];

echo $form->text('Headline');

echo $form->text('Description');

echo $form->repeater('Counter')->setFields([
    $form->image('Icon'),
    $form->row(
        $form->text('Number'),
        $form->text('Description'),
        $form->radio('Type')->setOptions($options)
    )
]);
