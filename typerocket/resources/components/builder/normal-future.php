<h1>Future Normal</h1>
<?php
$baseGroup = $form->getGroup();

$options = [
    'Yes' => true,
    'No' => false
];

echo $form->image('Image');
echo $form->radio('Padding')->setOptions($options);