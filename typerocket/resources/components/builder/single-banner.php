<h1>Single Banner</h1>
<?php
$baseGroup = $form->getGroup();

echo $form->text('Headline');

echo $form->textarea('Description');

echo $form->repeater('Images list')->setFields([
    $form->image('Image')
]);