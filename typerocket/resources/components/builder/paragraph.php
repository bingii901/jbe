<h1>Paragraph</h1>
<?php
$baseGroup = $form->getGroup();

$options_headline = [
    'On top' => true,
    'In line' => false
];

$options_background = [
    'Gray' => true,
    'Gray light' => false
];

echo $form->text('Headline');
echo $form->radio('Headline position')->setOptions($options_headline);
echo $form->radio('Background')->setOptions($options_background);

echo $form->editor('Paragraph');