<?php
/**
 * Understrap enqueue scripts
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

if ( ! function_exists( 'understrap_scripts' ) ) {
	/**
	 * Load theme's JavaScript and CSS sources.
	 */
	function understrap_scripts() {
		// Get the theme data.
		$the_theme     = wp_get_theme();
		$theme_version = $the_theme->get( 'Version' );

		$css_version = $theme_version . '.' . filemtime( get_template_directory() . '/assets/css/theme.min.css' );
		wp_enqueue_style( 'understrap-styles', get_template_directory_uri() . '/assets/css/theme.min.css', array(), $css_version );

		wp_enqueue_style('aos',get_template_directory_uri().'/assets/css/aos.css');
		wp_enqueue_style('swiper',get_template_directory_uri().'/assets/css/swiper-bundle.min.css');

		wp_enqueue_script( 'jquery' );
		wp_enqueue_script( 'jquery-script',get_template_directory_uri().'/assets/js/jquery-3.5.1.js' );

		$js_version = $theme_version . '.' . filemtime( get_template_directory() . '/assets/js/theme.min.js' );
		wp_enqueue_script( 'understrap-scripts', get_template_directory_uri() . '/assets/js/theme.min.js', array(), $js_version, true );

		wp_enqueue_script( 'aos', get_template_directory_uri() . '/assets/js/aos.js', array(), 'none', true );

		wp_enqueue_script( 'parallax', get_template_directory_uri() . '/assets/js/parallax.min.js', array(), 'none', true );

		wp_enqueue_script( 'swiper-script', get_template_directory_uri() . '/assets/js/swiper-bundle.min.js', array(), 'none', true );

        wp_enqueue_script( 'custom-script', get_template_directory_uri() . '/assets/js/customizer.js', array(), $js_version, true );
		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}
	}
} // endif function_exists( 'understrap_scripts' ).

add_action( 'wp_enqueue_scripts', 'understrap_scripts' );
