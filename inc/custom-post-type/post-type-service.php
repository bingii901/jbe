<?php
$service = tr_post_type('Service');
$service->setId('tr_service');
$service->setIcon('cube');
$service->setTitlePlaceholder('Enter full name here');
$service->setArgument('supports', ['title']);

//BODY
tr_meta_box('Future Details')
    ->setCallback(function () {
        $form = tr_form();
        echo $form->image('Icon');
    })->apply($service);

//CUSTOM TO SHOW TABLE
$service
    ->addColumn('icon', null, 'Icon', function ($value) {
        echo wp_get_attachment_image((int)$value, array(50,50));
    })
    ->removeColumn('date');