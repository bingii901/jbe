<?php
$project = tr_post_type('Project');
$project->setId('tr_project');
$project->setIcon('stack');
$project->setTitlePlaceholder('Enter full name here');
$project->setArgument('supports', ['title']);

//CATEGORY
tr_taxonomy('Projects', 'Category')
    ->apply($project)
    ->setHierarchical();

//BODY
tr_meta_box('Project Details')
    ->setCallback(function () {
        $form = tr_form();
        $editor = $form->editor('post_content');
        echo $editor->setLabel('About Project');
        echo $form->text('Address');
        echo $form->repeater('Parameters')->setFields([
            $form->row(
                $form->text('Name'),
                $form->text('Value')
            )
        ]);

        echo $form->repeater('Key futures')->setFields([
            $form->search('Search')->setPostType('tr_service')
        ]);
    })->apply($project);

//IMAGE
tr_meta_box('Image')
    ->setCallback(function () {
        $form = tr_form();
        echo $form->image('Thumbnail');
        echo $form->gallery('Gallery');
    })->apply($project);


//CUSTOM TO SHOW TABLE
$project
    ->addColumn('thumbnail', null, 'Thumbnail', function ($value) {
        echo wp_get_attachment_image((int)$value, array(100, 100));
    })
    ->addColumn($project->getId(), null, 'Category', function ($value) {
        $term = wp_get_post_terms(get_the_ID(), 'projects', array('fields' => 'all'));
        foreach ($term as $item) {
            echo '<strong><i>' . $item->name . '</i></strong>';
        }
    });