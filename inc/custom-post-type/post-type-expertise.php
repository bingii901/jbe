<?php
$project = tr_post_type('Expertise');
$project->setId('tr_expertise');
$project->setIcon('map');
$project->setTitlePlaceholder('Enter full name here');

//BODY
tr_meta_box('Expertise Details')
    ->setCallback(function () {
        $form = tr_form();
        $editor = $form->editor('post_insights');
        echo $editor->setLabel('Key Insights');
    })->apply($project);

tr_meta_box('Position')
    ->setCallback(function () {
        $form = tr_form();
        echo $form->textarea('Map Iframe');
    })->apply($project);

//IMAGE
tr_meta_box('Image')
    ->setCallback(function () {
        $form = tr_form();
        echo $form->image('Thumbnail');
        echo $form->image('Banner 1');
        echo $form->image('Banner 2');
    })->apply($project);

tr_meta_box("Future Project")
    ->setCallback(function () {
        $form = tr_form();
        echo $form->repeater('Futures Project')->setFields([
            $form->search('Search')->setPostType('tr_project')
        ]);
    });

//CUSTOM TO SHOW TABLE
$project
    ->addColumn('thumbnail', null, 'Thumbnail', function ($value) {
        echo wp_get_attachment_image((int)$value, array(100, 100));
    });