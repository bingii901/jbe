<?php
/**
 * Template Name: Product Detail
 *
 * Template for displaying a blank page.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined('ABSPATH') || exit;
?>
<!--START HEADER-->
<?php get_header() ?>
<!--END HEADER-->
<section class="project-detail-slide container-fluid">
    <div class="content">
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <?php
                if (!empty(tr_posts_field('gallery'))) {
                    foreach (tr_posts_field('gallery') as $item) { ?>
                        <!--START LOOP-->
                        <div class="swiper-slide">
                            <?= wp_get_attachment_image((int)$item, 'full'); ?>
                        </div>
                        <!--STOP LOOP-->
                    <?php }
                }
                ?>
            </div>
            <!-- Add Arrows -->
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
            <!-- Add Pagination -->
            <div class="swiper-pagination"></div>
        </div>
    </div>
</section>
<section class="project-detail-information container-fluid">
    <div class="content row">
        <div class="col-lg-6 information">
            <p class="text-uppercase"><?= tr_posts_field('address'); ?></p>
            <h3><?= get_post_field('post_title') ?></h3>
            <a class="btn btn-sm btn-primary radius-5" href="#">Executive Condo</a>
            <div class="description">
                <?php
                $query = get_post(get_the_ID());
                $content = apply_filters('the_content', $query->post_content);
                echo $content;
                ?>
            </div>
        </div>
        <div class="col-lg-6 parameter">
            <ul>
                <?php
                if (!empty(tr_posts_field('list_parameter'))) {
                    foreach (tr_posts_field('list_parameter') as $item) { ?>
                        <!--START LOOP-->
                        <li>
                            <p><?= $item['name'] ?></p>
                            <p><?= $item['value'] ?></p>
                        </li>
                        <!--STOP LOOP-->
                    <?php }
                }
                ?>
            </ul>
        </div>
    </div>
</section>
<section class="project-detail-key-future container-fluid">
    <div class="content">
        <h4>Key Features</h4>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the
            industry's standard dummy text ever since the 1500s</p>
        <div class="container">
            <ul class="list">
                <?php
                $services = tr_posts_field('key_futures');
                if (!empty($services))
                    foreach ($services as $item) {
                        $item = get_post($item['search']); ?>
                        <li>
                            <div class="icon">
                                <?= wp_get_attachment_image((int)$item->icon, 'full', true); ?>
                            </div>
                            <p><?= $item->post_title ?></p>
                        </li>
                        <?php
                    }
                ?>
            </ul>
            <div class="toggle-btn clearfix">
                <div class="btn arrow-open-close"></div>
            </div>
        </div>
    </div>
</section>
<section class="project-detail-relate container-fluid">
    <div class="content">
        <?php
        $prev_post = get_previous_post();
        if ($prev_post): ?>
            <a class="prev" href="<?= get_permalink($prev_post->ID); ?>">
                <div class="arrow">
                    <i class="fa fa-angle-left" aria-hidden="true"></i>
                </div>
                <div class="content">
                    <div class="text">
                        <h5>Next Project</h5>
                        <p><?= $prev_post->post_title ?></p>
                    </div>
                    <div class="thumbnail">
                        <?= wp_get_attachment_image((int)$prev_post->thumbnail, array(160,130)); ?>
                    </div>
                </div>
            </a>
        <?php endif; ?>
        <?php
        $next_post = get_next_post();
        if ($next_post): ?>
            <a class="next" href="<?= get_permalink($next_post->ID); ?>">
                <div class="content">
                    <div class="text">
                        <h5>Next Project</h5>
                        <p><?= $next_post->post_title ?></p>
                    </div>
                    <div class="thumbnail">
                        <?= wp_get_attachment_image((int)$next_post->thumbnail, array(160,130)); ?>
                    </div>
                </div>
                <div class="arrow">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                </div>
            </a>
        <?php endif; ?>
    </div>
</section>
<section class="contact-form container-fluid">
    <div class="content">
        <h3>Get In Touch</h3>
        <p>Have any enquiries? Fill up the form and we will get in touch as soon as possible.</p>

        <form class="form-label-float">
            <div class="form-row">
                <div class="form-group col-md-6" data-aos="fade-right">
                    <input type="name" class="form-control" id="fname" required placeholder="First Name">
                    <span data-toggle="First Name*" data-required="This field is required"></span>
                </div>
                <div class="form-group col-md-6" data-aos="fade-left">
                    <input type="name" class="form-control" id="lname" required placeholder="Last Name">
                    <span data-toggle="Last Name*" data-required="This field is required"></span>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6" data-aos="fade-right">
                    <input type="email" class="form-control" id="email" required placeholder="Email Address">
                    <span data-toggle="Email Address*" data-required="This field is required"></span>
                </div>
                <div class="form-group col-md-6" data-aos="fade-left">
                    <input type="phone" class="form-control" id="phone" required placeholder="Phone Number">
                    <span data-toggle="Phone Number*" data-required="This field is required"></span>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group" data-aos="fade-right">
                    <textarea type="text" class="form-control" id="message" required
                              placeholder="Messages..."></textarea>
                    <span data-toggle="Message..." data-required="This field is required"></span>
                </div>
            </div>
            <button type="submit" class="btn btn-submit" data-aos="fade-up">Submit</button>
        </form>
    </div>
</section>

<!--START FOOTER-->
<?php get_footer() ?>
<!--END FOOTER-->

<script>
    $('.btn.btn-scroll-top').click(function () {
        $([document.documentElement, document.body]).animate({
            scrollTop: $("body").offset().top - 140
        }, 2000);
    })

    var swiper = new Swiper('.swiper-container', {
        cssMode: true,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        pagination: {
            el: '.swiper-pagination'
        },
        mousewheel: true,
        keyboard: true,
    });

    $(document).on('click', '.toggle-btn', function () {
        $('.project-detail-key-future .list li').slideDown(500)
        $(this).toggleClass('hide-toggle');
    })

    $(document).on('click', '.toggle-btn.hide-toggle', function () {
        var c = $('.project-detail-key-future .list li').length
        $($('.project-detail-key-future .list li').get().reverse()).each(function () {
            $(this).slideUp(500)
            if (--c == 6)
                return false;
        });
        $(this).removeClass('hide-toggle');
    })
</script>