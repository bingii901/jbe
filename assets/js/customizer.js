/**
 * File customizer.js.
 *
 * Theme Customizer enhancements for a better user experience.
 *
 * Contains handlers to make Theme Customizer preview reload changes asynchronously.
 */

// Single banner
var swiper = new Swiper('.single-banner .swiper-container', {
    direction: 'vertical',
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },
    effect: 'fade',
    autoplay: {
        delay: 9000
    },
    speed: 2000,
}).autoplay.start()
