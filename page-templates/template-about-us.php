<?php
/**
 * Template Name: About Us Page
 *
 * Template for displaying a blank page.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined('ABSPATH') || exit;
get_header();
?>
<section class="about-page-heading container-fluid">
    <div class="content paragraph">
        <h1>About Us</h1>
        <div class="row">
            <div class="col-xl-6">
                <h4 data-aos="fade-up">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h4>
                <div data-aos="fade-down">
                    <p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining
                        essentially</p>
                    <br>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                        the
                        industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of
                        type.
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                        the
                        industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of
                        type.</p>
                </div>
                <br>
            </div>
            <div class="offset-xl-1 col-xl-5">
                <div class="image" data-aos="flip-down">
                    <img src="<?= get_template_directory_uri() ?>/assets/images/Rectangle%201978@2x.jpg" alt="">
                </div>
            </div>
        </div>
    </div>
</section>
<section class="about-page-statistic container-fluid bg-gray-light">
    <div class="content" data-aos="flip-up">
        <div class="heading">
            <h3>JBE in Numbers</h3>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the
                industry's standard dummy text ever since the 1500s</p>
        </div>
        <div class="box-parameter" data-aos="flip-up" data-aos-id="super-counter">
            <div class="item">
                <div class="icon">
                    <img src="<?= get_template_directory_uri() ?>/assets/images/icons/Group%2038@2x.png" alt="">
                </div>
                <div class="parameter">
                    <h3 class="counter-count plus">750</h3>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                </div>
            </div>
            <div class="item">
                <div class="icon">
                    <img src="<?= get_template_directory_uri() ?>/assets/images/icons/apartment@2x.png" alt="">
                </div>
                <div class="parameter">
                    <h3 class="counter-count plus">350</h3>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                </div>
            </div>
            <div class="item">
                <div class="icon">
                    <img src="<?= get_template_directory_uri() ?>/assets/images/icons/user%20(1)@2x.png" alt="">
                </div>
                <div class="parameter">
                    <h3 class="counter-count over">1000</h3>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="about-page-future">
    <div class="parallax-window" data-parallax="scroll"
         data-image-src="<?= get_template_directory_uri() ?>/assets/images/lance-anderson-QdAAasrZhdk-unsplash@2x.jpg"></div>
    <!--    <img src="-->
    <? //= get_template_directory_uri() ?><!--/assets/images/lance-anderson-QdAAasrZhdk-unsplash@2x.jpg" alt="">-->
</section>
<section class="about-page-heading container-fluid">
    <div class="content quote">
        <h4 data-aos="fade-up">Lorem ipsum</h4>
        <p data-author="Jonh Doe" data-aos="fade-down">“Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
            Ipsum has been the industry's standard dummy text ever since the 1500s.”</p>
    </div>
</section>
<section class="contact-form container-fluid">
    <div class="content">
        <h3>Get In Touch</h3>
        <p>Have any enquiries? Fill up the form and we will get in touch as soon as possible.</p>

        <form class="form-label-float">
            <div class="form-row">
                <div class="form-group col-md-6" data-aos="fade-right">
                    <input type="name" class="form-control" id="fname" required placeholder="First Name">
                    <span data-toggle="First Name*" data-required="This field is required"></span>
                </div>
                <div class="form-group col-md-6" data-aos="fade-left">
                    <input type="name" class="form-control" id="lname" required placeholder="Last Name">
                    <span data-toggle="Last Name*" data-required="This field is required"></span>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6" data-aos="fade-right">
                    <input type="email" class="form-control" id="email" required placeholder="Email Address">
                    <span data-toggle="Email Address*" data-required="This field is required"></span>
                </div>
                <div class="form-group col-md-6" data-aos="fade-left">
                    <input type="phone" class="form-control" id="phone" required placeholder="Phone Number">
                    <span data-toggle="Phone Number*" data-required="This field is required"></span>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group" data-aos="fade-right">
                    <textarea type="text" class="form-control" id="message" required
                              placeholder="Messages..."></textarea>
                    <span data-toggle="Message..." data-required="This field is required"></span>
                </div>
            </div>
            <button type="submit" class="btn btn-submit" data-aos="fade-up">Submit</button>
        </form>
    </div>
</section>
<!--START FOOTER-->
<?php get_footer() ?>
<!--END FOOTER-->

<script>


    $('.parallax-window').parallax();

    $('.btn.btn-scroll-top').click(function () {
        $([document.documentElement, document.body]).animate({
            scrollTop: $("body").offset().top - 140
        }, 2000);
    })

</script>