<?php
/**
 * Template Name: Contact Us
 *
 * Template for displaying a blank page.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
?>

<!--START HEADER-->
<?php get_header() ?>
<!--END HEADER-->

<section class="contact-page-form contact-form container-fluid">
    <div class="content row">
        <div class="image col-lg-6">
            <img src="<?= get_template_directory_uri() ?>/assets/images/lance-anderson-QdAAasrZhdk-unsplash@2x.jpg" alt="">
        </div>
        <div class="form offset-lg-1 col-lg-5">
            <h3>Get In Touch</h3>
            <p>Have any enquiries? Fill up the form and we will get in touch as soon as possible.</p>

            <form class="form-label-float">
                <div class="form-row">
                    <div class="form-group col-md-12" data-aos="fade-right">
                        <input type="name" class="form-control" id="fname" required placeholder="First Name">
                        <span data-toggle="First Name*" data-required="This field is required"></span>
                    </div>
                    <div class="form-group col-md-12" data-aos="fade-left">
                        <input type="name" class="form-control" id="lname" required placeholder="Last Name">
                        <span data-toggle="Last Name*" data-required="This field is required"></span>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12" data-aos="fade-right">
                        <input type="email" class="form-control" id="email" required placeholder="Email Address">
                        <span data-toggle="Email Address*" data-required="This field is required"></span>
                    </div>
                    <div class="form-group col-md-12" data-aos="fade-left">
                        <input type="phone" class="form-control" id="phone" required placeholder="Phone Number">
                        <span data-toggle="Phone Number*" data-required="This field is required"></span>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group" data-aos="fade-right">
                    <textarea type="text" class="form-control" id="message" required
                              placeholder="Messages..."></textarea>
                        <span data-toggle="Message..." data-required="This field is required"></span>
                    </div>
                </div>
                <button type="submit" class="btn btn-submit" data-aos="fade-up">Submit</button>
            </form>
        </div>
    </div>
</section>
<section class="contact-page-future container-fluid bg-gray-light">
    <div class="content row">
        <div class="col-lg-6">
            <h4>Need to Get a Hold of Us? <br>Reach Us Here</h4>
        </div>
        <div class="information col-lg-5 offset-lg-1">
            <div class="list">
                <h5>Phone</h5>
                <p>+65 6338 3616</p>
            </div>
            <div class="list">
                <h5>Address</h5>
                <p>6 Handy Road, #02-01, The Luxe <br>Singapore 229234</p>
            </div>
        </div>
    </div>
</section>
<!--START FOOTER-->
<?php get_footer() ?>
<!--END FOOTER-->
<script>
    $('.btn.btn-scroll-top').click(function () {
        $([document.documentElement, document.body]).animate({
            scrollTop: $("body").offset().top - 140
        }, 2000);
    })
</script>