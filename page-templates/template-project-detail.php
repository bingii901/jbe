<?php
/**
 * Template Name: Product Detail
 *
 * Template for displaying a blank page.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
?>
<!--START HEADER-->
<?php get_header() ?>
<!--END HEADER-->
<section class="project-detail-slide container-fluid">
    <div class="content">
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <img src="<?= get_template_directory_uri() ?>/assets/images/Rectangle%201954@2x.jpg" alt="">
                </div>
                <div class="swiper-slide">
                    <img src="<?= get_template_directory_uri() ?>/assets/images/Rectangle%201978@2x.jpg" alt="">
                </div>
            </div>
            <!-- Add Arrows -->
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
            <!-- Add Pagination -->
            <div class="swiper-pagination"></div>
        </div>
    </div>
</section>
<section class="project-detail-information container-fluid">
    <div class="content row">
        <div class="col-lg-6 information">
            <p class="text-uppercase">336, 338 PASIR PANJANG ROAD</p>
            <h3>Luxe Ville</h3>
            <a class="btn btn-sm btn-primary radius-5" href="#">Executive Condo</a>
            <div class="description">
                <p>
                    Completed in 2013, Luxe Ville is a freehold condominium development, comprised of 50 units, located
                    in Pasir Panjang. Condominium facilities include a swimming pool, BBQ pits, gymnasium, clubhouse,
                    function room, and playground.
                </p>
                <p>Luxe Ville is also closely located to esteemed educational and health institutions, including the
                    National University of Singapore (NUS), Anglo-Chinese School (Independent), and the National
                    University Hospital (NUH), as is a short bus ride or drive to Kent Ridge Park and Mapletree Business
                    City.
                </p>
            </div>
        </div>
        <div class="col-lg-6 parameter">
            <ul>
                <li>
                    <p>Tenure of Land</p>
                    <p>Freehold</p>
                </li>
                <li>
                    <p>Total Site Area (Sq ft)</p>
                    <p>41,850.08</p>
                </li>
                <li>
                    <p>Date of TOP</p>
                    <p>22 October 2012</p>
                </li>
                <li>
                    <p>Date of TOP</p>
                    <p>Residential</p>
                </li>
                <li>
                    <p>Property Type</p>
                    <p>50</p>
                </li>
                <li>
                    <p>Project Architect</p>
                    <p>Design link</p>
                </li>
            </ul>
        </div>
    </div>
</section>
<section class="project-detail-key-future container-fluid">
    <div class="content">
        <h4>Key Features</h4>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the
            industry's standard dummy text ever since the 1500s</p>
        <div class="container">
            <ul class="list">
                <li>
                    <div class="icon">
                        <img src="<?= get_template_directory_uri() ?>/assets/images/icons/Path%201044@2x.png" alt="">
                    </div>
                    <p>Gym</p>
                </li>
                <li>
                    <div class="icon">
                        <img src="<?= get_template_directory_uri() ?>/assets/images/icons/Group%2061142@2x.png" alt="">
                    </div>
                    <p>Pool</p>
                </li>
                <li>
                    <div class="icon">
                        <img src="<?= get_template_directory_uri() ?>/assets/images/icons/noun_Grill_255902@2x.png" alt="">
                    </div>
                    <p>Barbecue Pit</p>
                </li>
                <li>
                    <div class="icon">
                        <img src="<?= get_template_directory_uri() ?>/assets/images/icons/Group%2061141@2x.png" alt="">
                    </div>
                    <p>Valet Parking Service</p>
                </li>
                <li>
                    <div class="icon">
                        <img src="<?= get_template_directory_uri() ?>/assets/images/icons/noun_Tennis_2620313@2x.png" alt="">
                    </div>
                    <p>Tennis Court</p>
                </li>
                <li>
                    <div class="icon">
                        <img src="<?= get_template_directory_uri() ?>/assets/images/icons/noun_service_2328691@2x.png" alt="">
                    </div>
                    <p>Function Room</p>
                </li>
                <li>
                    <div class="icon">
                        <img src="<?= get_template_directory_uri() ?>/assets/images/icons/Path%201044@2x.png" alt="">
                    </div>
                    <p>Gym</p>
                </li>
                <li>
                    <div class="icon">
                        <img src="<?= get_template_directory_uri() ?>/assets/images/icons/Group%2061142@2x.png" alt="">
                    </div>
                    <p>Pool</p>
                </li>
                <li>
                    <div class="icon">
                        <img src="<?= get_template_directory_uri() ?>/assets/images/icons/noun_Grill_255902@2x.png" alt="">
                    </div>
                    <p>Barbecue Pit</p>
                </li>
                <li>
                    <div class="icon">
                        <img src="<?= get_template_directory_uri() ?>/assets/images/icons/Group%2061141@2x.png" alt="">
                    </div>
                    <p>Valet Parking Service</p>
                </li>
                <li>
                    <div class="icon">
                        <img src="<?= get_template_directory_uri() ?>/assets/images/icons/noun_Tennis_2620313@2x.png" alt="">
                    </div>
                    <p>Tennis Court</p>
                </li>
                <li>
                    <div class="icon">
                        <img src="<?= get_template_directory_uri() ?>/assets/images/icons/noun_service_2328691@2x.png" alt="">
                    </div>
                    <p>Function Room</p>
                </li>
            </ul>
            <div class="toggle-btn clearfix">
                <div class="btn arrow-open-close"></div>
            </div>
        </div>
    </div>
</section>
<section class="project-detail-relate container-fluid">
    <div class="content">
        <a class="prev">
            <div class="arrow">
                <i class="fa fa-angle-left" aria-hidden="true"></i>
            </div>
            <div class="content">
                <div class="text">
                    <h5>Next Project</h5>
                    <p>Signature at Yishun</p>
                </div>
                <div class="thumbnail">
                    <img src="<?= get_template_directory_uri() ?>/assets/images/Rectangle%201942@2x.jpg" alt="">
                </div>
            </div>
        </a>
        <a class="next">
            <div class="content">
                <div class="text">
                    <h5>Next Project</h5>
                    <p>Signature at Yishun</p>
                </div>
                <div class="thumbnail">
                    <img src="<?= get_template_directory_uri() ?>/assets/images/Rectangle%201956@2x.jpg" alt="">
                </div>
            </div>
            <div class="arrow">
                <i class="fa fa-angle-right" aria-hidden="true"></i>
            </div>
        </a>
    </div>
</section>
<section class="contact-form container-fluid">
    <div class="content">
        <h3>Get In Touch</h3>
        <p>Have any enquiries? Fill up the form and we will get in touch as soon as possible.</p>

        <form class="form-label-float">
            <div class="form-row">
                <div class="form-group col-md-6" data-aos="fade-right">
                    <input type="name" class="form-control" id="fname" required placeholder="First Name">
                    <span data-toggle="First Name*" data-required="This field is required"></span>
                </div>
                <div class="form-group col-md-6" data-aos="fade-left">
                    <input type="name" class="form-control" id="lname" required placeholder="Last Name">
                    <span data-toggle="Last Name*" data-required="This field is required"></span>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6" data-aos="fade-right">
                    <input type="email" class="form-control" id="email" required placeholder="Email Address">
                    <span data-toggle="Email Address*" data-required="This field is required"></span>
                </div>
                <div class="form-group col-md-6" data-aos="fade-left">
                    <input type="phone" class="form-control" id="phone" required placeholder="Phone Number">
                    <span data-toggle="Phone Number*" data-required="This field is required"></span>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group" data-aos="fade-right">
                    <textarea type="text" class="form-control" id="message" required
                              placeholder="Messages..."></textarea>
                    <span data-toggle="Message..." data-required="This field is required"></span>
                </div>
            </div>
            <button type="submit" class="btn btn-submit" data-aos="fade-up">Submit</button>
        </form>
    </div>
</section>

<!--START FOOTER-->
<?php get_footer() ?>
<!--END FOOTER-->

<script>
    $('.btn.btn-scroll-top').click(function () {
        $([document.documentElement, document.body]).animate({
            scrollTop: $("body").offset().top - 140
        }, 2000);
    })

    var swiper = new Swiper('.swiper-container', {
        cssMode: true,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        pagination: {
            el: '.swiper-pagination'
        },
        mousewheel: true,
        keyboard: true,
    });

    $(document).on('click', '.toggle-btn', function () {
        $('.project-detail-key-future .list li').slideDown(500)
        $(this).toggleClass('hide-toggle');
    })

    $(document).on('click', '.toggle-btn.hide-toggle', function () {
        var c = $('.project-detail-key-future .list li').length
        $($('.project-detail-key-future .list li').get().reverse()).each(function () {
            $(this).slideUp(500)
            if (--c == 6)
                return false;
        });
        $(this).removeClass('hide-toggle');
    })
</script>