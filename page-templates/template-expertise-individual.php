<?php
/**
 * Template Name: Expertise Individual
 *
 * Template for displaying a blank page.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
?>

<!--START HEADER-->
<?php get_header() ?>
<!--END HEADER-->
<section class="expertise-individual-heading container-fluid">
    <div class="content">
        <h1>Executive Condominiums</h1>
    </div>
</section>
<section class="expertise-individual-future">
    <img src="<?= get_template_directory_uri() ?>/assets/images/Herobanner@2x.jpg" alt="">
</section>
<section class="expertise-individual-heading container-fluid">
    <div class="content quote">
        <h3 data-author="Jonh Doe">"Lorem Ipsum is simply dummy text of the printing and typesetting industry."</h3>
    </div>
</section>
<section class="expertise-individual-executive container-fluid bg-gray-light">
    <div class="content">
        <div class="paragraph">
            <h5>Executive Condominiums</h5><br>
            <p>Lorem Ipsum is simply dummy text of<br> the printing and typesetting industry.</p><br>
            <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a
                galley of type and scrambled it to make a type specimen book.</p><br>
            <p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially
                unchanged.</p>
        </div>
    </div>
</section>
<section class="expertise-individual-executive container-fluid">
    <div class="content">
        <h4 class="heading">Key Insights</h4>
        <div class="paragraph">
            <p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining
                essentially.</p><br>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the
                industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type. </p>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the
                industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type. </p>
            <p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p><br>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type.</p>

        </div>
    </div>
</section>
<section class="expertise-individual-future container-fluid">
    <img src="<?= get_template_directory_uri() ?>/assets/images/Rectangle%201954@2x.jpg" alt="">
</section>
<section class="expertise-individual-heading container-fluid">
    <div class="content">
        <h4>Our Properties</h4>
    </div>
</section>
<section class="expertise-individual-map">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3988.7919413421228!2d103.84493461426545!3d1.2996435620979205!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31da19504d20e401%3A0x11b6e8bca29236eb!2sThe%20Luxe%20Apartments!5e0!3m2!1svi!2s!4v1600246819887!5m2!1svi!2s" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
</section>
<section class="expertise-individual-future-project container-fluid">
    <h5>Featured Project</h5>
    <div class="content row">
        <div class="item col-lg-3 col-md-6">
            <div class="thumbnail">
                <img src="<?= get_template_directory_uri() ?>/assets/images/handy-026238.jpg" alt="">
            </div>
            <div class="heading">
                <p>6 Handy Road</p>
                <h5>The Luxe</h5>
                <a class="btn btn-sm btn-primary" href="#">Apartment</a>
            </div>
        </div>
        <div class="item col-lg-3 col-md-6">
            <div class="thumbnail">
                <img src="<?= get_template_directory_uri() ?>/assets/images/Rectangle%201942@2x.jpg" alt="">
            </div>
            <div class="heading">
                <p>PASIR PANJANG ROAD</p>
                <h5>The Ville</h5>
                <a class="btn btn-sm btn-primary" href="#">Apartment</a>
            </div>
        </div>
        <div class="item col-lg-3 col-md-6">
            <div class="thumbnail">
                <img src="<?= get_template_directory_uri() ?>/assets/images/Rectangle%201956@2x.jpg" alt="">
            </div>
            <div class="heading">
                <p>INGGU ROAD / PENAGA PLACE</p>
                <h5>Terrace Houese</h5>
                <a class="btn btn-sm btn-primary" href="#">Apartment</a>
            </div>
        </div>
        <div class="item col-lg-3 col-md-6">
            <div class="thumbnail">
                <img src="<?= get_template_directory_uri() ?>/assets/images/FRM_4678.jpg" alt="">
            </div>
            <div class="heading">
                <p>346 SEMBAWANG CRESCENT</p>
                <h5>skypark residences</h5>
                <a class="btn btn-sm btn-primary" href="#">Apartment</a>
            </div>
        </div>
    </div>
</section>
<!--START FOOTER-->
<?php get_footer() ?>
<!--END FOOTER-->

<script>
    $('.btn.btn-scroll-top').click(function () {
        $([document.documentElement, document.body]).animate({
            scrollTop: $("body").offset().top - 140
        }, 2000);
    })
</script>