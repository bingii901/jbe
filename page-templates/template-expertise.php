<?php
/**
 * Template Name: Expertise
 *
 * Template for displaying a blank page.
 *
 * @package understrap
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
?>

<!--START HEADER-->
<?php get_header() ?>
<!--END HEADER-->
<section class="expertise-heading container-fluid">
    <div class="content">
        <h1>Our Expertise</h1>
        <p>JBE Properties is committed to enriching the lives of home owners in Singapore.</p>
    </div>
</section>
<section class="expertise-list container-fluid">
    <div class="content">
        <div class="item" data-href="#">
            <div class="item-box">
                <div class="heading">
                    <h5>Private Condo</h5>
                    <a class="btn btn-lg btn-primary" href="#">View More</a>
                </div>
                <div class="thumbnail">
                    <img src="<?= get_template_directory_uri() ?>/assets/images/project/Past%20Projects/Private%20Condo/The%20Luxe/6%20handy%20rd.jpg" alt="">
                </div>
            </div>
        </div>
        <div class="item" data-href="#">
            <div class="item-box">
                <div class="heading">
                    <h5>Executive Condo</h5>
                    <a class="btn btn-lg btn-primary" href="#">View More</a>
                </div>
                <div class="thumbnail">
                    <img src="<?= get_template_directory_uri() ?>/assets/images/project/Past%20Projects/EC/Signature%20at%20Yishun/IMG-20170826-WA0009.jpg" alt="">
                </div>
            </div>
        </div>
        <div class="item" data-href="#">
            <div class="item-box">
                <div class="heading">
                    <h5>Landed Housing</h5>
                    <a class="btn btn-lg btn-primary" href="#">View More</a>
                </div>
                <div class="thumbnail">
                    <img src="<?= get_template_directory_uri() ?>/assets/images/project/Past%20Projects/Landed%20Housing/Terrace%20houses.PNG" alt="">
                </div>
            </div>
        </div>
    </div>
</section>
<!--START FOOTER-->
<?php get_footer() ?>
<!--END FOOTER-->
<script>
    $('.btn.btn-scroll-top').click(function () {
        $([document.documentElement, document.body]).animate({
            scrollTop: $("body").offset().top - 140
        }, 2000);
    })
</script>