<div class="collapse navbar-collapse flex-row-reverse" id="navbarNav">
    <ul class="navbar-nav">
        <li class="nav-item active">
            <a class="nav-link" href="http://<?php echo $_SERVER['SERVER_NAME'] ?>/jbe/about-us">About Us</a>
        </li>
        <li class="nav-item">
            <div class="nav-link btn-group">
                <a href="http://<?php echo $_SERVER['SERVER_NAME'] ?>/jbe/our-projects">Our Project</a>
                <button type="button" class="btn btn-link dropdown-toggle dropdown-toggle-split"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                </button>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="http://<?php echo $_SERVER['SERVER_NAME'] ?>/jbe/project-detail">Project Detail</a>
                    <a class="dropdown-item" href="http://<?php echo $_SERVER['SERVER_NAME'] ?>/jbe/our-projects-interaction">Interaction</a>
                </div>
            </div>
        </li>
        <li class="nav-item">
            <div class="nav-link btn-group">
                <a href="http://<?php echo $_SERVER['SERVER_NAME'] ?>/jbe/expertise">Our Expertise</a>
                <button type="button" class="btn btn-link dropdown-toggle dropdown-toggle-split"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                </button>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="http://<?php echo $_SERVER['SERVER_NAME'] ?>/jbe/expertise-individual">Individual</a>
                </div>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="http://<?php echo $_SERVER['SERVER_NAME'] ?>/jbe/contact-us">Contact Us</a>
        </li>
    </ul>
</div>